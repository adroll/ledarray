#ifndef MAX522_H
#define MAX522_H

#include <Arduino.h>
#include <SPI.h>

class Max522{

private:
  float fRefVoltage;
  int fSlaveSelectPin;

  void SetRegister(int pControl, int pData);


public:
  Max522(int pSlaveSelect, float pRefVoltage);

  void Init();
  void SetSlaveSelectPin(int pPin);
  void SetRefVoltage(float pVoltage);


  void SetA(int pValue);
  void SetB(int pValue);
  void SetAB(int pValue);

  void ShutdownA();
  void ShutdownB();
  void ShutdownAB();
};




#endif
