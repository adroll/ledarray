#include <Arduino.h>
#include <ArduinoJson.h>
#include <SPI.h>
#include <Max522.h>
#include <MCP3008.h>

const int fClockPin = 13;
const int fMisoPin = 12;
const int fMosiPin = 11;

const int fMaxSlaveSelect = 10;
const int fMCPSlaveSelect = 9;

const int fADCPinA = A0;
const int fADCPinB = A1;
const int fADCPinLED = A2;
const float fVRef = 5.0;


//resistance of the measurement resistor in Ohm
const uint8_t fMeasResChan = 2;
float fMeasRes = 1000;



Max522 fMax522 = Max522(fMaxSlaveSelect, fVRef);
MCP3008 fADC = MCP3008(fClockPin, fMosiPin, fMisoPin, fMCPSlaveSelect);

/*
  see this nice page: http://denethor.wlu.ca/pc364/arduino/Arduino_SPI_da_lab.shtml

  Do the following configurations with your Arduino UNO:
  PIN10: CS (slave select)
  PIN11: DIN
  PIN13: SCLK
  ---- not PIN12 is not used since only Data is sent ----

  VDD = REF = 5V (can also be 3.3V)
*/

bool perform_command(String pCommand, String pPayload)
{
  StaticJsonDocument<200> sendData;
  sendData["command"] = pCommand;
  sendData["payload"] = pPayload;


  // Serial.println("BLA");
  // Serial.println(pCommand);
  // Serial.println(pPayload);
  // Serial.print("{");
  // Serial.print(" \"payload\": \"" + pPayload + "\",");
  // Serial.print(" \"command\": \"" + pCommand + "\",");
  bool valid = false;
  String message = "\"Command <"+pCommand+"> not found\"";
  if(pCommand == "setAB")
  {
    fMax522.SetAB(pPayload.toInt());
    valid = true;
  }
  if(pCommand == "setA")
  {
    fMax522.SetA(pPayload.toInt());
    valid = true;
  }
  if(pCommand == "setB")
  {
    fMax522.SetB(pPayload.toInt());
    valid =  true;
  }
  if(pCommand == "getADCA")
  {
    int val = analogRead(fADCPinA);
    // Serial.print(" \"result\": " + String(val) + ",");
    sendData["result"] = val;
    valid = true;
  }
  // if(pCommand == "getVA")
  // {
  //   int val = analogRead(fADCPinA);
  //   Serial.print(" \"result\": ");
  //   Serial.print(String(val*5.0/1024., cDecimalPlaces));
  //   Serial.print(",");
  //   valid = true;
  // }
  if(pCommand == "getADCB")
  {
    int val = analogRead(fADCPinB );
    sendData["result"] = val;
    valid = true;
  }
  // if(pCommand == "getVB")
  // {
  //   int val = analogRead(fADCPinB);
  //   Serial.print(" \"result\": ");
  //   Serial.print(String(val*5.0/1024., cDecimalPlaces));
  //   Serial.print(",");
  //   valid = true;
  // }
  if(pCommand == "getMeasRes")
  {
    sendData["result"] = fMeasRes;
    valid = true;
  }
  if(pCommand == "setMeasRes")
  {
    fMeasRes = pPayload.toFloat();
    sendData["result"] = fMeasRes;
    valid = true;
  }

  if(pCommand == "getRefVoltage")
  {
    sendData["result"] = fVRef;
    valid = true;
  }

  if(pCommand == "getADC")
  {
    uint8_t cChan = pPayload.toInt();
    uint16_t val = fADC.readChan(cChan);
    sendData["result"] = val;
    valid = true;
  }
  if(pCommand == "getArdLEDADC")
  {
    int val = analogRead(fADCPinLED );
    sendData["result"] = val;
    valid = true;
  }
  if(pCommand == "Fast")
  {
    //Do all measurements in once:
    int val = analogRead(fADCPinLED );
    sendData["ArdLedADC"] = val;
    uint16_t cVal = fADC.readChan(2);
    sendData["MCPLedADC"] = cVal;
    val = analogRead(fADCPinA );
    sendData["ArdBaseADC"] = val;
    cVal = fADC.readChan(0);
    sendData["MCPBaseADC"] = cVal;
    valid = true;
  }

  // if(pCommand == "getV")
  // {
  //   uint8_t cChan = pPayload.toInt();
  //   uint16_t val = fADC.readChan(cChan);
  //   Serial.print(" \"result\": ");
  //   Serial.print(String(val*fVRef/1024., cDecimalPlaces));
  //   Serial.print(",");
  //   valid = true;
  // }

  // if(pCommand == "getLEDCurrent")
  // {
  //   uint16_t val = fADC.readChan(fMeasResChan);
  //   float cCurrent = val*fVRef/1024./fMeasRes;
  //   Serial.print(" \"result\": ");
  //   Serial.print(String(cCurrent, cDecimalPlaces) );
  //   Serial.print(",");
  //   valid = true;
  // }


  // Check if the command and readback was valid:
  sendData["valid"] = valid;
  if(!valid)
  {
    sendData["message"] = message;
  }
  serializeJson(sendData, Serial);
  Serial.println();
  return valid;
}


void setup() {
  // put your setup code here, to run once:
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(fADCPinA, INPUT);
  pinMode(fADCPinB, INPUT);
  Serial.begin(115200);
  // Serial.println("----- Setup ------");

  //Initialize the pins:

  // Serial.println("Initialize SPI slave pins:");
  // MCP3008:
  pinMode(fMCPSlaveSelect, OUTPUT);
  digitalWrite(fMCPSlaveSelect, HIGH);
  // Serial.print(" - MCP3008: Pin ");
  // Serial.println(fMCPSlaveSelect);
  //MAX522:
  pinMode(fMaxSlaveSelect, OUTPUT);
  digitalWrite(fMaxSlaveSelect, HIGH);
  // Serial.print(" - MAX522:  Pin ");
  // Serial.println(fMaxSlaveSelect);

  //Start the SPI Transfer
  SPI.begin();
  // Serial.println("----------------");

}





void loop() {
  // put your main code here, to run repeatedly

  //check for commands:
  int length = Serial.available();
  if (length > 0) { // data available on HW Serial
    // Serial.println("Received Command");
    String inData = Serial.readStringUntil('\n');
    // Serial.println("read: " + inData);

    /*
    Serial.println("----------------------");
    Serial.println("received Data: " + inData);

    int val = 0;
    val = analogRead(fAnaPinRead);
    Serial.print("Read Arduino ADC value: ");
    Serial.print(val);
    Serial.print(" = ");
    Serial.print(val/1024.*fVRef);
    Serial.println("V");
    Serial.println("Read IC ADC values: ");
    for(uint8_t cChan = 0; cChan < 8; cChan++)
    {
      uint16_t result = fADC.readChan(cChan);
      Serial.print("  - Ch. ");
      Serial.print(cChan);
      Serial.print(": ");
      Serial.print(result);
      Serial.print(" = ");
      Serial.print(result/1024.*fVRef);
      Serial.println("V");
    }
    */

    int cSepPos = inData.indexOf(":");
    if(cSepPos != -1)
    {
      String cCommand = inData.substring(0, inData.indexOf(":"));
      String cPayload = inData.substring(cSepPos+1);
      cPayload.replace("\n","");
      // Serial.println("command: " + cCommand);
      // Serial.println("payload: " + cPayload);
      perform_command(cCommand, cPayload);
      // if(!valid)
      // {
      //   Serial.println("{");
      //   Serial.println(" 'message': 'Command not found',");
      //   Serial.println(" 'result': False,");
      //   Serial.println("}");
      // }
    }

    else
    {
      Serial.print("{");
      Serial.print(" \"message\":  \"Incorrect format, use <command>:<payload>\",");
      Serial.print(" \"valid\": false");
      Serial.println("}");
    }
  }
}
