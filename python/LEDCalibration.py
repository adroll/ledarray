import pandas as pd
import time
from datetime import datetime
import numpy as np
import matplotlib.pyplot as plt
from LEDArray import LEDArray


def record_calibration():
    print("Perform calibration measurement: ")
    port = "/dev/ttyACM0"
    baudrate = 115200
    samples = 5
    resistor = 100.

    myled = LEDArray(port=port, baudrate=baudrate, resistor=resistor)

    resistor = myled.getMeasRes()
    print(f" - Measurement Resistor: {resistor}Ohm")
    refvoltage = myled.getRefVoltage()
    print(f" - Reference Voltage: {refvoltage}V")

    now = datetime.now()
    date_time = now.strftime("%d-%m-%Y_%Hh%Mm%Ss")

    starttime = time.time()

    res = pd.DataFrame()
    DACs = np.arange(0,256,1)
    # DACs = np.arange(105, 115,5)
    print(DACs)
    try:
        for DAC in DACs:

            print("----------------------")
            print(f"    DAC = {DAC}")
            myled.setLEDDAC(DAC)
            for sample in range(samples):
                data = {}
                nowtime = time.time() - starttime
                data["time"] = nowtime
                data["DAC"] = DAC
                # print(f"   - Sample: {sample}")
                # print(f"      time = {nowtime}s")
                data["resistor"] = resistor
                data["refvoltage"] = refvoltage

                dat = myled.getFast()
                data["base_adc"] = dat["MCPBaseADC"]
                data["arduino_base_adc"] = dat["ArdBaseADC"]
                data["led_adc"] = dat["MCPLedADC"]
                data["arduino_led_adc"] = dat["ArdLedADC"]

                # data["base_adc"] = myled.getBaseADC()
                # data["arduino_base_adc"] = myled.getArdBaseADC()
                # data["led_adc"] = myled.getLEDADC()
                # data["arduino_led_adc"] = myled.getArdLEDADC()
                current = data["led_adc"] * refvoltage/1028./resistor
                # print(f"      I_LED = {current*1000}mA")

                # print(data)
                res = res.append(data, ignore_index=True)
    except:
        print("failed")
    myled.setLEDDAC(0)
    print(res)
    res.to_csv(f"Results/calibration_{date_time}_{refvoltage}V_{resistor}Ohm.csv", index=False)
    return res

def analyze(result):
    result["resistor_voltage"] = result["led_adc"]*result["refvoltage"]/1028.
    result["base_voltage"] = result["base_adc"]*result["refvoltage"]/1028.
    result["led_current"] = result["resistor_voltage"]/result["resistor"]

    print(result)

    ########### LED ADC over DAC
    plt.plot(result["DAC"], result["led_adc"], label="MCP3008")
    plt.plot(result["DAC"], result["arduino_led_adc"], label="Arduino")
    plt.plot(result["DAC"], result["base_adc"], "--", label="MCP3008 (Base)")
    plt.plot(result["DAC"], result["arduino_base_adc"], "--", label="Arduino (Base)")
    plt.legend()
    plt.xlabel("DAC")
    plt.ylabel("ADC")
    plt.show()
    plt.clf()

    ############# LED ADC over time
    plt.plot(result["time"], result["led_adc"], label="MCP3008")
    plt.plot(result["time"], result["arduino_led_adc"], label="Arduino")
    plt.legend()
    plt.xlabel("time (s)")
    plt.ylabel("ADC")
    plt.show()
    plt.clf()

    ### Resistor voltage and base voltage over time
    plt.plot(result["time"], result["resistor_voltage"], label="Resistor Voltage (MCP3008)")
    plt.plot(result["time"], result["base_voltage"], label="Base Voltage (MCP3008)")
    plt.legend()
    plt.xlabel("time (s)")
    plt.ylabel("voltage (V)")
    plt.show()
    plt.clf()


if __name__ == "__main__":
    result = record_calibration()
    # filename = "Results/calibration_11-09-2020_15h18m33s_5.0V_100.0Ohm.csv"
    # result = pd.read_csv(filename)
    analyze(result)
