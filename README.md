# LED Array Control

## Parameters:
Those parameters are hard coded in the main file and define control stuff:
- `fMaxSlaveSelect`: Slave select PIN for the **Max522** IC
- `fMCPSlaveSelect`: Slave select PIN for the **MCP3008** IC
- `fClockPin`: Clock Pin for the SPI connection
- `fMisoPin`: MISO (Master In Slave Out) Pin for the SPI connection
- `fMosi`: MOSI (Master Out Slave In) Pin for the SPI connection
- `fVRef`: Reference voltage in V for the ADC and DAC (5.0V for the Arduino Uno)
- `fMeasRes`: Measurement Resistor in Ohm for determing the LED current
- `fLEDChan`: channel of the MCP3008 ADC to measure the voltage drop over the measurement resistor


## Commands:
The commands have the format `<command>:<value>` while the value is optional but the : has to be given
- `setAB:<value>`: set the DAC value [0,255] to set the LED current (channel A is connected to the constant current source)
- `setA:<value>`/`setB:<value>`: set the DAC values [0,255] for the single DAC channels
- `getDACA:`/`getDACB:`: get the DAC value from the **MAX522** for channel A/B
- `getADC:<channel>`: get the ADC value for the Channel [0,7], while 0 is connected the measurement resistor
- `getV:<channel>`: get the calculated voltage for the channel [0,7] of the **MCP3008** with respect to the reference voltage `fVRef`
- `getLEDCurrent:`: returns the current of the LED in Ampere calculated with the reference voltage `fVRef` and measurement resistor `fMeasRes`
- `getLEDCurrent`: reads the LED current
