#include "Max522.h"



Max522::Max522(int pSlaveSelect = 10, float pRefVoltage = 5.0)
{
  SetRefVoltage(pRefVoltage);
  SetSlaveSelectPin(pSlaveSelect);
}

void Max522::Init()
{
  ShutdownAB();
}

void Max522::SetRefVoltage(float pVoltage)
{
  fRefVoltage = pVoltage;
}

void Max522::SetSlaveSelectPin(int pPin)
{
  fSlaveSelectPin = pPin;
}


void Max522::SetA(int pValue)
{
  SetRegister(0x21, pValue);
}

void Max522::SetB(int pValue)
{
  SetRegister(0x22, pValue);
}

void Max522::SetAB(int pValue)
{
  SetRegister(0x23, pValue);
}

void Max522::ShutdownA()
{
  SetRegister(0x28,0);
}

void Max522::ShutdownB()
{
  SetRegister(0x30,0);
}

void Max522::ShutdownAB()
{
  SetRegister(0x38,0);
}

void Max522::SetRegister(int pControl, int pData)
{
  digitalWrite(fSlaveSelectPin, LOW);
  //  send in the control and data via SPI:
  SPI.transfer(pControl);
  SPI.transfer(pData);
  // take the SS pin high to de-select the chip:
  digitalWrite(fSlaveSelectPin, HIGH);
}
