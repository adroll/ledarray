import serial
import time
import json


class LEDArray:
    def __init__(self, port="/dev/ttyACM0", baudrate=115200, timeout=0.1, resistor=None):
        print("Init LED array")
        self._port = port
        self._baudrate = baudrate
        self._connection = serial.Serial(port, baudrate, timeout=timeout)
        self._connection.reset_input_buffer()
        self._ledchan = 2
        self._basechan = 0
        time.sleep(2)
        self.setLEDDAC(0)
        if resistor != None:
            self.setMeasRes(resistor)

    def ask_json(self, command):
        self._connection.write(f"{command}\n".encode("utf-8"))
        data = self._connection.readline()[:-2] #the last bit gets rid of the new-line chars
        counter = 0
        while(data == b"" and counter < 100):
            # print("readline", counter)
            counter+=1
            data = self._connection.readline()[:-2] #the last bit gets rid of the new-line chars
        dat = json.loads(data.decode("utf-8"))
        return dat

    def setLEDDAC(self, dac):
        dat = self.ask_json(f"setA:{dac}")
        if not dat["valid"]:
            print(dat["message"])

    def getRefVoltage(self):
        dat = self.ask_json("getRefVoltage:")
        if dat["valid"]:
            return dat["result"]
        print(dat["message"])

    def getMeasRes(self):
        dat = self.ask_json("getMeasRes:")
        if dat["valid"]:
            return dat["result"]
        print(dat["message"])

    def setMeasRes(self, resistor):
        dat = self.ask_json(f"setMeasRes:{resistor}")
        if dat["valid"]:
            print(dat)
            return dat["result"]
        print(dat["message"])

    def getBaseADC(self):
        dat = self.ask_json(f"getADC:{self._basechan}")
        if dat["valid"]:
            return dat["result"]
        print(dat["message"])

    def getArdBaseADC(self):
        dat = self.ask_json("getADCA:")
        if dat["valid"]:
            return dat["result"]
        print(dat["message"])

    def getFast(self):
        dat = self.ask_json("Fast:")
        if dat["valid"]:
            return dat
        print(dat["message"])

    def getLEDADC(self):
        dat = self.ask_json(f"getADC:{self._ledchan}")
        if dat["valid"]:
            return dat["result"]
        print(dat["message"])

    def getArdLEDADC(self):
        dat = self.ask_json("getArdLEDADC:")
        if dat["valid"]:
            return dat["result"]
        print(dat["message"])

if __name__ == "__main__":
    print("Test Serial")
    port = "/dev/ttyACM0"
    baudrate = 115200
    myled = LEDArray(port=port, baudrate=baudrate)

    # print("reference voltage: ", myled.getRefVoltage(), "V")
    # print("Measurement Resistor: ", myled.getMeasRes(), "Ohm")
    # print("GetLEDCurrent")
    # myled.setLEDDAC(0)
    # adc = myled.getLEDADC()
    # print(f"MCP3008: {adc}ADC = {adc*5.0/1028.}")
    # adc = myled.getArdLEDADC()
    # print(f"Arduino: {adc}ADC = {adc*5.0/1028.}")
    #
    # myled.setLEDDAC(128)
    # adc = myled.getLEDADC()
    # print(f"MCP3008: {adc}ADC = {adc*5.0/1028.}")
    # adc = myled.getArdLEDADC()
    # print(f"Arduino: {adc}ADC = {adc*5.0/1028.}")
    #
    # myled.setLEDDAC(255)
    # adc = myled.getLEDADC()
    # print(f"MCP3008: {adc}ADC = {adc*5.0/1028.}")
    # adc = myled.getArdLEDADC()
    # print(f"Arduino: {adc}ADC = {adc*5.0/1028.}")

    command = input("Enter command: ")
    while (command != "exit"):
        data = myled.ask_json(command)
        print(data)
        command = input("Enter command: ")

    exit()
